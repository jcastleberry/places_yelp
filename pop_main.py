import requests
import logging
import numpy
import json
import sys
import copy
import yelp_gen
from time import sleep
from dict_builder import make_master_dict
from zip_gen import Zipgen


def main():

    def logger_setup():
        log_format = '%(asctime)-s [%(levelname)-s] (%(threadName)-s)  %(message)s'
        logging.basicConfig(format=log_format, filename='pop_main.log', level=logging.DEBUG)

    logger_setup()
    logging.info('##########   POP TIMES MAIN STARTED   ##########')
    arg_len = len(sys.argv)
    cmd_args = sys.argv

    if arg_len < 3:
        print()
        print('ERROR:')
        print(r'To few arguments, please specify the City, State, and Category')
        print(r'Yelp Categories are listed at https://www.yelp.com/developers/documentation/v3/all_category_list')
        print(r"""Example Usage: python3 pop_main.py FL MIAMI {'Nightlife': 'bars, All'} bars""")

    else:
        state = sys.argv[1]
        city = sys.argv[2]
        # Remove the City and State from cmd_args so a category dict can be created
        cat_args = cmd_args[3:arg_len]
        search_term = sys.argv[4]
        logging.info("State: " + state)
        logging.info("City: " + city)
        logging.info("Category: " + str(cat_args))

    def make_category(cat_args):
        # create category dict
        cat_dict = dict()
        num_args = len(cat_args)
        key = cat_args[0]
        vals = cat_args[1:num_args]
        cat_dict[key] = ', '.join(vals)
        return cat_dict

    category = make_category(cat_args)

    master_dict = make_master_dict()
    logging.info('Creating Master Dictionary')
    logging.info(str(master_dict))
    logging.info('##########   ' + 'BEGIN ZIP CODE CALLS FOR ' + city.upper() + ' ' + state.upper() + '   ##########')
    zip_generator = Zipgen()
    zip_code_json = zip_generator.request_zips(city_name=city, state_name=state)
    with open(state + '_' + city + '_' + 'zip_codes.json', 'w') as z:
        json.dump(zip_code_json, z)
    logging.info('ZIP CODE API CALL COMPLETE')

    # Begin Yelp API Calls for each zip code
    yelp_requestor = yelp_gen.DataGen()

    def calc_num_requests(offset_num):
        if offset_num < 50:
            calls = 1
            return calls
        else:
            z = offset_num / 50
            x = numpy.ceil(z)
            return int(x)

    # # Holds Yelp Location names to be used for Google API calls
    name_list = []
    yelp_dict = {}
    locations = []


    for code in zip_code_json['zip_codes']:
        num_biz = yelp_requestor.yelp_requester(search=search_term,
                                                category=category,
                                                city=city,
                                                state=state,
                                                zip_code=code,
                                                flag='o',
                                                offset=950)
        num_api_calls = calc_num_requests(num_biz)
        logging.info('number of calls to make:  ' + str(num_api_calls))
        location_ct = 0
        for i in range(0, num_api_calls):
            offset = i * 50
            location_data = yelp_requestor.yelp_requester(search=search_term,
                                                          category=category,
                                                          city=city,
                                                          state=state,
                                                          zip_code=code,
                                                          flag='s',
                                                          offset=offset)
            for location in location_data:
                loc_name = location['name']
                closed_status = location['is_closed']
                if closed_status is True:
                    pass
                else:
                    name_list.append(loc_name)
                    try:
                        details = dict()
                        details['image_url'] = location['image_url']
                        details['id'] = location['id']
                        details['url'] = location['url']
                        details['price'] = location['price']
                        details['review_count'] = location['review_count']
                        details['rating'] = location['rating']
                        details['categories'] = location['categories']
                        details['transactions'] = location['transactions']
                        details['coordinates'] = location['coordinates']
                        details['location'] = location['location']
                        details['address'] = location['location']
                        details['image_url'] = location['image_url']
                        details['phone'] = location['phone']
                        yelp_dict[loc_name] = details
                        locations.append(loc_name)
                        if loc_name not in name_list:
                            name_list.append(loc_name)
                    except KeyError:
                        pass

                location_ct = location_ct + 1
            logging.info("Number of locations: " + str(location_ct))
            logging.info("Number of Results: " + str(location_ct))
            logging.debug('Sleeping 1 Second')
            sleep(1)
    print()


    # Requests category data for each zip code
    yelp_dict.pop('alias', None)
    yelp_dict.pop('is_closed', None)
    yelp_dict.pop('url', None)
    yelp_dict.pop('image_url', None)
    yelp_dict.pop('distance', None)

    for k, v in yelp_dict.items():
        print(k, v)
        print()
    locations = {}

    def create_dict_from_yelp_loc_data(loc):
        try:
            loc_dict = copy.deepcopy(master_dict)
            loc_dict['yelp_id'] = yelp_dict[loc]["id"]
            loc_dict['category'] = yelp_dict[loc]["categories"]
            loc_dict['address'] = yelp_dict[loc]["address"]
            loc_dict['phone'] = yelp_dict[loc]["phone"]
            loc_dict['city'] = yelp_dict[loc]["location"]["city"]
            loc_dict['zip_code'] = yelp_dict[loc]["location"]["zip_code"]
            loc_dict['yelp_image_url'] = yelp_dict[loc]["url"]
            loc_dict['yelp_price_level'] = yelp_dict[loc]['price']
            loc_dict['yelp_total_reviews'] = yelp_dict[loc]["review_count"]
            loc_dict['yelp_rating'] = yelp_dict[loc]["rating"]
            loc_dict['latitude'] = yelp_dict[loc]["coordinates"]["latitude"]
            loc_dict['Longitude'] = yelp_dict[loc]["coordinates"]["longitude"]
            locations[loc] = loc_dict
        except KeyError:
            pass

    # BEGIN GOOGLE API CALLS

    logging.info('#################### BEGIN GOOGLE PLACES REQUEST FOR IDs ####################')

    google_api_key = 'AIzaSyCrvJhX8Z7-R080_fGf4tSWA2XmYtDqpBQ'

    def get_loc_id(loc_name):
        formatted_search_str = loc_name.replace(' ', '%')
        url = 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?'
        query_input_type = '&inputtype=textquery'
        query = '&input=' + city + '&' + formatted_search_str
        query_fields = '&fields=formatted_address,' \
                       'opening_hours,' \
                       'geometry,' \
                       'icon,' \
                       'place_id,' \
                       'name,' \
                       'permanently_closed,' \
                       'photos,' \
                       'place_id,' \
                       'plus_code,' \
                       'types'

        key = '&key=' + google_api_key
        req_str = url + query_input_type + query + query_fields + key
        r = requests.get(req_str, timeout=5)
        logging.debug(loc_name + ' Response OK: ' + str(r.status_code))
        resp_data = r.json()
        candidate_data_list = resp_data.get("candidates")
        place_id = (candidate_data_list[0]['place_id'])
        logging.info('ID RESPONSE: ' + loc_name + ',' + place_id)
        return resp_data

    location_data_dict = {}
    for name in name_list:
        print('Getting ID Data for:  ' + name)
        logging.info('Getting ID Data for:  ' + name)
        data = get_loc_id(name)
        location_data_dict[name] = data

    print(location_data_dict)
    with open(state + '_' + city + '_' + 'GooglePlaceIDs.json', 'w') as id_file:
        json.dump(location_data_dict, id_file)




if __name__ == '__main__':
    main()
