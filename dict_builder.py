# Generates a dictionary for all key/value pairs from google and yelp
def make_master_dict():
    location_data_dict = dict.fromkeys(['category',
                                       'google_id',
                                       'yelp_id',
                                       'address',
                                       'city',
                                       'zip_code',
                                       'phone',
                                       'opening_hours',
                                       'website',
                                       'yelp_image_url',
                                       'yelp_price_level',
                                       'yelp_rating',
                                       'yelp_total_reviews',
                                       'latitude',
                                       'Longitude',
                                       'map_link',
                                       'google_popular_times'])
    return location_data_dict