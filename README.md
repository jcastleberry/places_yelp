### INSTRUCTIONS ###
1. Place the 4 files (pop_main.py, yelp_gen.py, zip_gen.py and dict_builder.py) into a folder)
2. Browse to that folder via Command Line or Bash
3. Run the script command with arguements


### Command Line Arguments ###

Example command for Miami:
python3 pop_main.py FL MIAMI {'Nightlife': 'bars, All'} bars

### Requirements ###
Python 3

Modules: requests, numpy

