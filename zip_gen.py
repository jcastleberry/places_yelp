import requests
import logging


class Zipgen:

    def __init__(self, city_name=None, state_name=None):

        self.city_name = city_name
        self.state_name = state_name

    def request_zips(self, city_name, state_name):
        """
        Calls the zipcodeapi.com API for all zip codes within a city

        :param city_name: Name of the city to request zip codes for
        :param state_name: Name of the state that the city resides in
        :return: A dict of zip codes. Key is the city name, Value is the list of codes
        """

        logging.info('ZIPGEN: city=' + city_name)
        logging.info('ZIPGEN: state=' + state_name)

        logging.info('Requesting Zip Codes for ' + city_name)
        api_key = 'vd9nZ8oI7OrG2pVwPzs08MpZeOdM2HZ68bsQ5bGeFRTA7YQgk55RmZQUISWo9VE8'
        url = ('https://www.zipcodeapi.com/rest/' + api_key + '/city-zips.json/' + city_name + '/' + state_name)
        r = requests.get(url)
        logging.debug(r)
        data = r.json()
        return data